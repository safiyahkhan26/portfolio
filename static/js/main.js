// JavaScript File
var button1 = document.getElementById('button-one');
var column1 = document.getElementById('col-one');
var button1Close = document.getElementById('button-one-close');

//when the button is clicked the function is executed 
button1.onclick = function(){
    button1.style.display = "none";
    column1.style.display = "block";
    button1Close.style.display = "block";
};

button1Close.onclick = function(){
    button1Close.style.display = "none";
    column1.style.display = "none";
    button1.style.display = "block";
};

var button2 = document.getElementById('button-two');
var column2 = document.getElementById('col-two');
var button2Close = document.getElementById('button-two-close');

button2.onclick = function(){
    button2.style.display ="none";
    column2.style.display ="block";
    button2Close.style.display ="block";
};

button2Close.onclick= function(){
    button2Close.style.display = "none";
    column2.style.display ="none";
    button2.style.display ="block";
}

var button3 = document.getElementById('button-three');
var column3 = document.getElementById('col-three');
var button3Close = document.getElementById('button-three-close');

button3.onclick = function(){
    button3.style.display ="none";
    column3.style.display ="block";
    button3Close.style.display ="block";
};

button3Close.onclick = function(){
    button3Close.style.display ="none";
    column3.style.display ="none";
    button3.style.display ="block";
}

function startTime() {
    var today = new Date();
    var hr = today.getHours();
    var min = today.getMinutes();
    var sec = today.getSeconds();
    min = checkTime(min);
    sec = checkTime(sec);
    document.getElementById('Clock').innerHTML = hr + ":" + min + ":" + sec;
    var t = setTimeout(startTime, 500);
}
function checkTime(i) {
    if (i < 10) {i = "0" + i};
    return i;
}